#!/bin/bash

# v0.3 (April 2018) by Andrew Davis (addavis@gmail.com)
#
# Version history:
#   - Mar/Apr 2018 : updates to work with 3D brains, changed argument structure,
#       detects ASSET, forward warp uses correct axis and no smoothing, icorr,
#       does reg to check results, runs in parallel, updated documentation
#   - Nov 2012 : code split from fieldmap script

# Distributed under The MIT License (MIT).
#   See http://opensource.org/licenses/MIT for details.

# TODO:
#   - use gz flag similarly to fieldmap to write intermediate files as .nii and gzip at end
#   - add noout flag to just write the pixelshift maps, not the unwarped volume(s)
#   - remove comments related to bet and intensity normalization once new script is created
#   - check out mainfeatreg preprocessFieldmaps function (sigloss section)
#       -- apply sigloss to field-map mag to improve reg with epi
#   - write a separate script that applies the pixelshift, along with motion correction reg
#       and normalization transforms, using convertwarp/applywarp (see FNIRT guide)

function printUsage {
cat << EOF

    $(basename $0)
    ---------
   
    This program performs distortion correction of gradient echo EPI images.
      It uses the Fugue tool from FSL, employing a B0 field-map, such as one
      created with the 'fieldmap' script. The script takes care of registration
      between a hi-res structural image, the fieldmap, and the EPI, and also
      does forward-warping of the fieldmap in the manner of Cusack et al. 
      (2003; doi: 10.1006/nimg.2002.1281).
    By default, with '-u g', the script unwarps along the phase encode axis
      in both directions. The output png images should be inspected to choose
      the correctly unwarped EPI. Quicklook on the nii images is also useful.
    This script has some biases. For one, it can detect ASSET acceleration
      from GE data, but does not detect acceleration from other vendors.
    If this is fully 3D data (e.g. whole brain), use the '-3d' flag.
    N.B. input data should be motion corrected before applying this script.

    Requires:
      - FSL (https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FslInstallation)
      - AFNI (https://afni.nimh.nih.gov/pub/dist/doc/htmldoc/background_install/install_instructs)
      - optional: .bash_script_messages in home dir (ask Andrew)

    Usage: $(basename $0) [options] struct_nii epi_dir epi_nii B0map_nii

    Required positional arguments:
      struct_nii : NIfTI of hi-res structural (skull stripped, if brain image)
         epi_dir : directory of EPI dicom(s) (for dicom header values)
         epi_nii : NIfTI of (motion corrected) EPI data
       B0map_nii : NIfTI fieldmap.  Use B0map-rads file (units rad/s)

    Options:
           --3d : input data is fully 3d (e.g. whole brain images)
        --accel : set this if acceleration was on during acquisition (assumes x2)
       -u uwdir : unwarp direction (default g, i.e. +/- PE direction) [x,x-,y,y-,z,z-,g]
        --icorr : intensity correct EPI images at the unwarp step
       --chkreg : do feat-style registration to check unwarping performance
      --progims : generate check images at various stages of the process
      --noclean : keep temporary files

    Outputs:
        {epi_nii}-pxlshft_{uwdir} : pixel shift map in EPI space 
             {epi_nii}-uw_{uwdir} : EPI NIfTI, unwarped along given axis(es)
          {epi_nii}-uw_check_ims/ : (with -progims) pngs to check unwarp progress and success
      {epi_nii}-uw_{uwdir}.uwreg/ : (with -chkreg) 6 DOF registration to check unwarp success

    Example 1; Slab of few slices:
        $(basename $0) t1w_leg-am Ser4/ epi gre454-B0map-rads

    Example 2; 3D whole-brain images:
        $(basename $0) --3d t1w_struct_brain Ser4/ epi gre5-B0map-rads

    Example 3; With intensity correction and checking registration:
        $(basename $0) --3d --icorr --chkreg t1w_struct_brain Ser4/ epi gre5-B0map-rads

    Example 4; EPI data acquired with acceleration on a Siemens or Philips scanner:
        $(basename $0) --3d --accel t1w_struct_brain Ser4/ epi gre5-B0map-rads

EOF
}

# Robust script options
set -o nounset    # fail on unset variables
set -o errexit    # fail on non-zero return values
set -o pipefail   # fail if any piped command fails

if [[ $# -eq 0 ]] || [[ $1 == -h ]] || [[ $1 == -help ]] || [[ $1 == --help ]]; then
    printUsage
    exit 0
fi

# Parallel jobs management
max_jobs=$(getconf _NPROCESSORS_ONLN)
function check_wait_jobs {
    while [[ $(jobs -p | wc -w) -ge $1 ]]; do
        wait -n
    done
}

# Red error messages, etc
[[ -f ~/.bash_script_messages ]] \
  && source ~/.bash_script_messages \
  || scriptEcho() { shift $(($#-1)); echo "$1"; }


# Default values
docleanup=True
threedee=False
accel=False
uwdir="g"
doregchk=False
doicorr=False
doprogchk=False

# Parse options
while [ $# -gt 4 ]; do
    case $1 in
           --3d ) threedee=True;;
        --accel ) accel=True;;
             -u ) uwdir="$2"; shift;;
        --icorr ) doicorr=True;;
       --chkreg ) doregchk=True;;
      --progims ) doprogchk=True;;
      --noclean ) docleanup=False;;
              * ) scriptEcho -e "Unrecognized option '$1'"; exit 1;;
    esac
    shift
done

# Parse required args
structNii="$1" && shift
epiDir="${1%/}" && shift
epiNii="$1" && shift
fieldmap="$1" && shift

# Check for valid args
if [[ -z "$structNii" ]] || [[ $(imtest "$structNii") -ne 1 ]]; then
    scriptEcho -e "missing or invalid structural nifti"
    exit 1
elif [[ -z "$epiDir" ]] || [[ ! -d "$epiDir" ]]; then
    scriptEcho -e "missing or invalid epi directory"
    exit 1
elif [[ -z "$epiNii" ]] || [[ $(imtest "$epiNii") -ne 1 ]]; then
    scriptEcho -e "missing or invalid epi nifti"
    exit 1
elif [[ -z "$fieldmap" ]] || [[ $(imtest "$fieldmap") -ne 1 ]]; then
    scriptEcho -e "missing or invalid fieldmap nifti"
    exit 1
else
    structPref=$($FSLDIR/bin/remove_ext "$structNii")    # file prefix w/o .nii or .nii.gz
    structSN="${structPref%%-*}"                         # series number, or whatever is before a dash, else the whole prefix
    epi=$($FSLDIR/bin/remove_ext "$epiNii")
    epiSN="${epi%%-*}"
    fm=$($FSLDIR/bin/remove_ext "$fieldmap")
    fmSN="${fm%%-*}"
    if [[ $threedee == True ]]; then
        fmMag="${fm%rads}mag_brain"
    else
        fmMag="${fm%rads}mag"
    fi
    [[ $(imtest "${fmMag}") -eq 1 ]] || { scriptEcho "file not found: $fmMag"; exit 2; }

    tmpDir=$(mktemp -d -t "ca.spinup.$(basename $0)_pid$$.XXXXX")
    [[ -d "${tmpDir}" ]] || { scriptEcho -e "Problem creating temp directory: ${tmpDir}"; exit 2; }
    # mkdir -p "$tmpDir"

    if [[ $doprogchk == True ]]; then
        chkDir="${epi}-uw_check_ims"
        mkdir -p "$chkDir"
    fi
fi

# Determine EPI TE, TR, dwell time, and ASSET
epiDicom="$(find "${epiDir}" -name '[^.]*.MR.dcm' -print -quit)"
epiTE=$(dicom_hinfo -no_name -tag 0018,0081 "$epiDicom")
epiTR=$(dicom_hinfo -no_name -tag 0018,0080 "$epiDicom")

epiDwellus=$(dicom_hinfo -no_name -tag 0043,102c "$epiDicom")
epiDwells=$(python -c "print(${epiDwellus}/10.0**6)")

# This assumes an axial acquisition
epiPE=$(dicom_hinfo -no_name -tag 0018,1312 "$epiDicom")
if [ $epiPE = ROW ]; then
   epiPE="R-L"
   if [ $uwdir = g ]; then uwdir="x x-"; fi
elif [ $epiPE = COL ]; then
   epiPE="P-A"
   if [ $uwdir = g ]; then uwdir="y y-"; fi
fi

epiAssetHdr=$(dicom_hinfo -no_name -tag 0043,1084 "$epiDicom")
epiAssetHdr2=$(dicom_hinfo -no_name -tag 0043,1083 "$epiDicom")
if [[ ${epiAssetHdr: -15} == "asset\YES\ASSET" ]] && [[ ${epiAssetHdr2} == "0.5\1" ]]; then
    scriptEcho "ASSET detected."
    accel=True
fi

if [[ $accel == True ]]; then
    epiDwellus=$(python -c "print($epiDwellus/2.0)")
    epiDwells=$(python -c "print($epiDwells/2.0)")
else
    scriptEcho -w "ASSET not detected and accel=False. Relevant header lines are:"
    echo "    ${epiAssetHdr}"
    echo "    ${epiAssetHdr2}"
fi

scriptEcho "Detected parameters from EPI dicom:"
echo "    EPI TE = $epiTE ms"
echo "    EPI TR = $epiTR ms"
echo "    EPI echo spacing = $epiDwellus µs"
echo "    EPI PE direction = $epiPE"


# This scheme forward-distorts the fieldmap image so it can be applied to the EPI.
# First, register struct to fieldmap and transform it
scriptEcho "Co-registering struct and fieldmap ..."
flirt_args=(-cost corratio -interp spline -datatype float)
if [[ $threedee == True ]]; then
    # 3D, 6dof reg
    flirt_args+=(-dof 6)
    xfm_ext="6dof.xfm"
else
    # 2D, 3dof reg
    flirt_args+=(-2D -schedule $FSLDIR/etc/flirtsch/sch2D_3dof)
    xfm_ext="3dof.xfm"
fi
flirt -in "$fmMag" -ref "$structPref" \
      "${flirt_args[@]}" \
      -omat "${tmpDir}/${fmSN}_to_${structSN}-${xfm_ext}" \
      -out "${tmpDir}/${fmSN}-in_${structSN}_space"

convert_xfm -omat "${tmpDir}/${structSN}_to_${fmSN}-${xfm_ext}" \
            -inverse "${tmpDir}/${fmSN}_to_${structSN}-${xfm_ext}"

flirt -in "$structPref" -ref "$fmMag" \
      ${flirt_args[@]} \
      -applyxfm -init "${tmpDir}/${structSN}_to_${fmSN}-${xfm_ext}" \
      -out "${tmpDir}/${structSN}-in_${fmSN}_space"

if [[ $doprogchk == True ]]; then
    if [[ $threedee == True ]]; then
        slicer_args=(-e 0.05 -a)
    else
        slicer_args=(-e 0.2 -A 768)
    fi

    slicer "$structPref" "${tmpDir}/${fmSN}-in_${structSN}_space" \
           ${slicer_args[@]} "$chkDir/${fmSN}-in_${structSN}_space.png"
    if [[ $threedee == True ]]; then
        slicer "$structPref" "${tmpDir}/${fmSN}-in_${structSN}_space" \
               -e 0.05 -x 0.42 "$chkDir/${fmSN}-in_${structSN}_space_sag.png"
    fi
fi

# Get mean EPI for later registration steps
fslmaths "$epi" -Tmean "${tmpDir}/${epi}-Tmean" -odt float

# Function to forward warp undistorted images and unwarp the EPIs
function unwarp_on {
    # This all has to do be done per axis because forward warping has to be in the right direction too
    #   the lone arg is the unwarp axis
    axis=$1

    # axis names for Fugue are x, x-, y, y-; determine correct opposite
    #  this is just for me -- Fugue determines correct opposite internally
    if [[ ${axis: -1} == - ]]; then
        oppaxis=${axis:0:1}
    else
        oppaxis=${axis}-
    fi


    # Distort struct and co-register with EPI mean image
    # N.B. asym is difference between the two TEs (s), whereas dwell (echo spacing)
    #   is the time between acqusition of k-space lines in the EPI image. asym is
    #   *not* required for doing the unwarping, only creating the fieldmap.
    scriptEcho "Warping struct and fieldmap on $oppaxis to match EPI (in parallel) ..."
    fugue_args=(--dwell=$epiDwells --unwarpdir=$axis)

    # commented these smoothing args -- bad idea, through hardly made a difference
    # if [[ $threedee == True ]]; then
    #     fugue_args+=(--smooth3=10)
    # else
    #     fugue_args+=(--smooth2=10)
    # fi

    fugue -i "${tmpDir}/${structSN}-in_${fmSN}_space" --loadfmap="$fm" \
          ${fugue_args[@]} \
          -w "${tmpDir}/${structSN}-in_${fmSN}_space-warped_${axis}"

    flirt -in "${tmpDir}/${epi}-Tmean" \
          -ref "${tmpDir}/${structSN}-in_${fmSN}_space-warped_${axis}" \
          "${flirt_args[@]}" \
          -omat "${tmpDir}/${epiSN}_to_${structSN}_in_fm_warped_${axis}_space.xfm" \
          -out "${tmpDir}/${epiSN}-in_${structSN}_fm_warped_${axis}_space"

    convert_xfm -omat "${tmpDir}/${structSN}_in_fm_warped_${axis}_space_to_${epiSN}.xfm" \
                -inverse "${tmpDir}/${epiSN}_to_${structSN}_in_fm_warped_${axis}_space.xfm"

    if [[ $doprogchk == True ]]; then
        [ $threedee == True ]] && slicer_args=(-e 0.1 -a)
        slicer "${tmpDir}/${structSN}-in_${fmSN}_space-warped_${axis}" \
               "${tmpDir}/${epiSN}-in_${structSN}_fm_warped_${axis}_space" \
               ${slicer_args[@]} "$chkDir/${epiSN}-in_${structSN}_fm_warped_${axis}_space.png"
        if [[ $threedee == True ]]; then
            slicer "${tmpDir}/${structSN}-in_${fmSN}_space-warped_${axis}" \
                   "${tmpDir}/${epiSN}-in_${structSN}_fm_warped_${axis}_space" \
                   -e 0.1 -x 0.42 "$chkDir/${epiSN}-in_${structSN}_fm_warped_${axis}_space_sag.png"
        fi
    fi


    # Warp and transform fieldmap to EPI size
    fugue_args+=(--nokspace)

    fugue -i "$fm" --loadfmap="$fm" \
          ${fugue_args[@]} \
          -w "${tmpDir}/${fm}-warped_${axis}"



    # consider the following sigloss code from mainfeatreg:
    
    # get a sigloss estimate and make a siglossed mag for forward warp
    # epi_te=`awk "BEGIN {print $echoTime / 1000.0}"`
    # runCommand $1 "${FSLDIR}/bin/sigloss -i FM_UD_fmap --te=$epi_te -m FM_UD_fmap_mag_brain_mask -s FM_UD_fmap_sigloss"
    # siglossthresh=`awk "BEGIN {print 1.0 - ( $signalLossThreshold / 100.0 )}"`
    # runCommand $1 "${FSLDIR}/bin/fslmaths FM_UD_fmap_sigloss -mul FM_UD_fmap_mag_brain FM_UD_fmap_mag_brain_siglossed -odt float"    
    

    # runCommand $1 "${FSLDIR}/bin/fslmaths FM_UD_fmap_sigloss -thr $siglossthresh FM_UD_fmap_sigloss -odt float"
    # runCommand $1 "${FSLDIR}/bin/overlay 1 0 FM_UD_fmap_mag_brain -a FM_UD_fmap_sigloss 0 1 FM_UD_sigloss+mag"    

    # createSmallSliceReport FM_UD_sigloss+mag $2 $1  "<p>Thresholded signal loss weighting image<br>
    # <a href=\"reg/unwarp/FM_UD_sigloss+mag.png\"><IMG BORDER=0 SRC=\"reg/unwarp/FM_UD_sigloss+mag.png\" WIDTH=1200></a>"





    flirt -in "${tmpDir}/${fm}-warped_${axis}" \
          -ref "${tmpDir}/${epi}-Tmean" \
          ${flirt_args[@]} \
          -applyxfm -init "${tmpDir}/${structSN}_in_fm_warped_${axis}_space_to_${epiSN}.xfm" \
          -out "${tmpDir}/${fm}-warped_${axis}_in_${epiSN}_space"

    if [[ $doprogchk == True ]]; then
        slicer "${tmpDir}/${fm}-warped_${axis}_in_${epiSN}_space" \
               "${tmpDir}/${epi}-Tmean" \
               ${slicer_args[@]} "$chkDir/${fm}-warped_${axis}_in_${epiSN}_space.png"
    fi


    # Unwarp EPI (Finally!)
    scriptEcho "Unwarping EPI on axis ${axis} ..."
    fugue_args=(--dwell=$epiDwells --unwarpdir=$axis)
    [[ $doicorr == True ]] && fugue_args+=(--icorr)
    echo "    fugue options are: ${fugue_args[@]}"

    fugue -i "$epi" --loadfmap="${tmpDir}/${fm}-warped_${axis}_in_${epiSN}_space" \
          "${fugue_args[@]}" \
          --saveshift="${epi}-pxlshft_${axis}" \
          -u "${epi}-uw_${axis}"
    

    # Check results using Slicer
    if [[ $doprogchk == True ]]; then
        if [[ $threedee == True ]]; then
            slicer_args=(-e 0.15 -a)
        else
            slicer_args=(-e 0.2 -A 768)
        fi
        flirt -in "${epi}-uw_${axis}" \
              -ref "$structPref" \
              "${flirt_args[@]}" \
              -omat "${tmpDir}/${epiSN}_to_${structSN}_for_display.xfm"

        convert_xfm -omat "${tmpDir}/${structSN}_to_${epiSN}_for_display.xfm" \
                    -inverse "${tmpDir}/${epiSN}_to_${structSN}_for_display.xfm"

        flirt -in "$structPref" -ref "${epi}-uw_${axis}" \
              ${flirt_args[@]} \
              -applyxfm -init "${tmpDir}/${structSN}_to_${epiSN}_for_display.xfm" \
              -out "${tmpDir}/${structPref}-in_${epiSN}_space"

        fslmaths "${epi}-uw_${axis}" -Tmean "${tmpDir}/${epi}-uw_${axis}_Tmean" -odt float

        slicer "${tmpDir}/${structPref}-in_${epiSN}_space" \
               "${tmpDir}/${epi}-uw_${axis}_Tmean" \
               ${slicer_args[@]} "$chkDir/${epi}-unwarp_check_${axis}.png"
        slicer "${tmpDir}/${epi}-uw_${axis}_Tmean" \
               "${tmpDir}/${structPref}-in_${epiSN}_space" \
               ${slicer_args[@]} "$chkDir/${epi}-unwarp_check2_${axis}.png"
        if [[ $threedee == True ]]; then
            slicer "${tmpDir}/${epi}-uw_${axis}_Tmean" \
                   "${tmpDir}/${structPref}-in_${epiSN}_space" \
                   -e 0.15 -x 0.42 "$chkDir/${epi}-unwarp_check2_${axis}_sag.png"
        fi
    fi
}

# Unwarp along each axis requested
for a in $uwdir; do
    unwarp_on $a &

    # spawn 1 job per core at most
    check_wait_jobs $max_jobs
done
wait

if [[ $doprogchk == True ]]; then
    # Generate Slicer from original for comparison
    scriptEcho "Generating slicer image from original ..."
    flirt -in "$epi" \
          -ref "$structPref" \
          "${flirt_args[@]}" \
          -omat "${tmpDir}/${epiSN}_to_${structSN}_for_display.xfm"

    convert_xfm -omat "${tmpDir}/${structSN}_to_${epiSN}_for_display.xfm" \
                -inverse "${tmpDir}/${epiSN}_to_${structSN}_for_display.xfm"

    flirt -in "$structPref" -ref "${tmpDir}/${epi}-Tmean" \
          ${flirt_args[@]} \
          -applyxfm -init "${tmpDir}/${structSN}_to_${epiSN}_for_display.xfm" \
          -out "${tmpDir}/${structPref}-in_${epiSN}_space"

    slicer "${tmpDir}/${structPref}-in_${epiSN}_space" \
           "${tmpDir}/${epi}-Tmean" \
           ${slicer_args[@]} "$chkDir/${epi}-unwarp_check_orig.png"
    slicer "${tmpDir}/${epi}-Tmean" \
           "${tmpDir}/${structPref}-in_${epiSN}_space" \
           ${slicer_args[@]} "$chkDir/${epi}-unwarp_check2_orig.png"
    if [[ $threedee == True ]]; then
        slicer "${tmpDir}/${epi}-Tmean" \
               "${tmpDir}/${structPref}-in_${epiSN}_space" \
               -e 0.15 -x 0.42 "$chkDir/${epi}-unwarp_check2_orig_sag.png"
    fi
fi


# Do feat-style 6 DOF registration of both unwarped directions and the original EPI to
#   to the GRE mag image, so we can check the edge images and determine the correct
#   unwarping direction.
if [[ $doregchk == True ]]; then
    # get number of temporal volumes and choose middle one as FEAT does
    vol_cnt=$(fslval "$epi" dim4 | tr -d [:blank:])
    ex_vol=$(python -c "print('{:d}'.format(int(${vol_cnt}/2.0)))") # works like floor()

    # function to emulate feat registration with its nice html report
    function epi_chkreg {
        local im="$1"
        local regdir="${im}.uwreg"

        # Create feat working dir with html/css files and example_func
        if [[ -d "$regdir" ]]; then
            scriptEcho "Skipping check reg on $im ..."
            return 0
        elif [[ $(imtest "$im") -eq 0 ]]; then
            scriptEcho -e "$im not found"
            exit 2
        else
            scriptEcho "Running check reg on $im (in parallel) ..."
            mkdir "$regdir"
        fi

        mkdir "$regdir"/.files
        cp $FSLDIR/doc/fsl.css "$regdir"/.files/
        cp -r $FSLDIR/doc/images "$regdir"/.files/images

        fslroi "$im" "$regdir"/example_func $ex_vol 1
        imcp "$fmMag" "$regdir"/fm_mag
        cd "$regdir"

        # Mask example_func
        if [[ $threedee == True ]]; then
            # Skull strip EPI -- note my mask is more liberal then feat's f=0.3
            bet example_func mask -f 0.2 -n -m -R
            immv mask_mask mask

            # Feat algorithm for bet and intensity normalization:

            # fslmaths prefiltered_func_data_mcf -Tmean mean_func
            # bet2 mean_func mask -f 0.3 -n -m
            # immv mask_mask mask
            # fslmaths prefiltered_func_data_mcf -mas mask prefiltered_func_data_bet
            # fslstats prefiltered_func_data_bet -p 2 -p 98
            # -0.000000 6284.523438
            # fslmaths prefiltered_func_data_bet -thr 628.4523438 -Tmin -bin mask -odt char
            # fslstats prefiltered_func_data_mcf -k mask -p 50
            # 5265.583008
            # fslmaths mask -dilF mask
            # fslmaths prefiltered_func_data_mcf -mas mask prefiltered_func_data_thresh
            # fslmaths prefiltered_func_data_thresh -mul 1.89912493732 prefiltered_func_data_intnorm
            # fslmaths prefiltered_func_data_intnorm filtered_func_data
            # fslmaths filtered_func_data -Tmean mean_func
            # /bin/rm -rf prefiltered_func_data*

        else
            # Automask unwarped EPIs
            # 3dAutomask -q -clfrac 0.2 -dilate 2 -prefix "$mask.nii.gz" "example_func.nii.gz"
            # Not exactly the same, since it doesn't use 3dClipLevel, -gradual etc:
            fslmaths example_func -thrP 20 -bin -kernel 2D -ero -dilF -dilF -fillh mask -odt int
        fi

        fslmaths example_func -mul mask example_func -odt float


        # Reg EPI to B0-map mag
        flirt_args=(-cost corratio -interp spline -datatype float)
        if [[ $threedee == True ]]; then
            # 3D, 6dof reg
            flirt_args+=(-dof 6)
            flirt_args+=(-searchrx -45 45 -searchry -45 45 -searchrz -45 45)
        else
            # 2D, 3dof reg
            flirt_args+=(-2D -schedule $FSLDIR/etc/flirtsch/sch2D_3dof)
        fi

        # Mimic the function of mainfeatreg
        #   N.B. in mainfeatreg, -w gives DOF arg, which can also take 7
        #   for global scale, and the (evil) value BBR
        flirt -in example_func \
              -ref fm_mag \
              ${flirt_args[@]} \
              -omat example_func2fm_mag.xfm \
              -out example_func2fm_mag

        # convert_xfm -inverse \
        #             -omat fm_mag2example_func.xfm \
        #             example_func2fm_mag.xfm

        slicer example_func2fm_mag \
               fm_mag \
               -s 2 \
               -x 0.35 sla.png -x 0.45 slb.png -x 0.55 slc.png -x 0.65 sld.png \
               -y 0.35 sle.png -y 0.45 slf.png -y 0.55 slg.png -y 0.65 slh.png \
               -z 0.35 sli.png -z 0.45 slj.png -z 0.55 slk.png -z 0.65 sll.png

        pngappend sla.png + slb.png + slc.png + sld.png \
                - sle.png + slf.png + slg.png + slh.png \
                - sli.png + slj.png + slk.png + sll.png \
                example_func2fm_mag1.png

        slicer fm_mag \
               example_func2fm_mag \
               -s 2 \
               -x 0.35 sla.png -x 0.45 slb.png -x 0.55 slc.png -x 0.65 sld.png \
               -y 0.35 sle.png -y 0.45 slf.png -y 0.55 slg.png -y 0.65 slh.png \
               -z 0.35 sli.png -z 0.45 slj.png -z 0.55 slk.png -z 0.65 sll.png

        pngappend sla.png + slb.png + slc.png + sld.png \
                - sle.png + slf.png + slg.png + slh.png \
                - sli.png + slj.png + slk.png + sll.png \
                example_func2fm_mag2.png

        pngappend example_func2fm_mag1.png + example_func2fm_mag2.png example_func2fm_mag.png

        /bin/rm -f sl?.png example_func2fm_mag1.png example_func2fm_mag2.png

        cat <<-"EOF" > report_reg.html
		<HTML>
		<HEAD>
		<link REL="stylesheet" TYPE="text/css" href=".files/fsl.css">
		<TITLE>EPIUNWARP</TITLE>
		</HEAD>
		<BODY>
		<h2>Registration</h2>
		
		<hr>
		<p><b>Analysis methods</b><br>
		EPI distortion correction was carried out using <b>epiunwarp</b> [Davis, 2016]. Then a 6 DOF registration to the field-map magnitude image was performed, using FLIRT [Jenkinson 2001, 2002].

		<p><b>References</b><br>
		Davis, Andrew D., and Michael D. Noseworthy. "Motion and distortion correction of skeletal muscle echo planar images." Magnetic resonance imaging 34.6 (2016): 832-838. DOI: <a href="http://doi.org/10.1016/j.mri.2016.03.003">10.1016/j.mri.2016.03.003</a><br>
		<hr>
		
		<p>Registration of example_func to fm_mag<br>
		<a href="example_func2fm_mag.png"><IMG BORDER=0 SRC="example_func2fm_mag.png" WIDTH=2000></a><br>
		</BODY>
		</HTML>
		EOF


        # Measure corratio of results (smaller is better = less btw group variance)
        tmpFile=$(mktemp)
        mkdir costs/
        for costfn in corratio mutualinfo normmi leastsq; do
            flirt -in example_func2fm_mag \
                  -ref fm_mag \
                  -cost $costfn \
                  -schedule $FSLDIR/etc/flirtsch/measurecost1.sch \
                  > $tmpFile
            reg_metric=$(head -n 1 $tmpFile | cut -d ' ' -f 1) # had to use this intermediate file to prevent a pipefail exit

            echo "$reg_metric" > costs/${costfn}.txt
        done
        /bin/rm "$tmpFile"
        cd ..
    }


    # For the orig EPI and each unwarped EPI, generate the registration to check results
    dir_list=()
    for epi_to_chk in "$epi" $(for axis in $uwdir; do printf "${epi}-uw_${axis} "; done); do
        dir_list+=("${epi_to_chk}.uwreg")
        epi_chkreg "$epi_to_chk" &

        # spawn 1 job per core at most
        check_wait_jobs $max_jobs
    done
    wait

    # Also create an animated gif in the epi dir
    # cd "${dir_list[0]}"
        # whirlgif -o EF_UD_movie.gif -time 50 -loop 0 EF_D_edges.gif EF_U_edges.gif

        # "<p>Movie of undistorted and distorted ( marked with a red dot ) ${1} images<br>
        # <a href=\"reg/unwarp/EF_UD_movie.gif\"><IMG BORDER=0 SRC=\"reg/unwarp/EF_UD_movie.gif\" WIDTH=1200></a>
        # <br>"
    # cd ..

    # Report cost functions to check results
    for d in ${dir_list[@]}; do
        scriptEcho "Registration metrics for ${d%.uwreg}:"
        for costfn in corratio mutualinfo normmi leastsq; do
            m=$(cat "${d}/costs/${costfn}.txt")
            printf "             %12s: %01.4f\n" ${costfn} $m
        done
        scriptEcho "Check ${d}/report_reg.html for visual results"
    done
fi


# Clean up temp files
if [[ $docleanup == True ]]; then
    scriptEcho "Deleting ${tmpDir} ..."
    /bin/rm -rf "$tmpDir"
else
    scriptEcho "Temp files are in $tmpDir"
fi
