#!/bin/bash

# v0.4.0 (April 2018) by Andrew Davis (addavis@gmail.com)
#
# Version history:
#   - 2018 : use dcm2niix instead of rundcm2nii/dcm2nii, improve documentation; code overhaul for 3D;
#              use removeramps in prelude, reduce blur and mask of phase images; added registration of
#              GRE inputs; revised smoothing/regularization of phase and field maps
#   - 2017 : updates to work better with brains in 3D
#   - 2012 : pretty much a total re-write
#   - 2009 : do the scaling after map generation to avoid artifacts at the wrap boundaries; re-written for bash
#   -  --  : original script in csh by Adrian Koziak for use at the Brain Body Imaging Center

# Distributed under The MIT License (MIT).
#   See http://opensource.org/licenses/MIT for details.

# TODO:
#   - implement slice-at-a-time (may be better for EPI since it is really acquired slice-by-slice). This
#       requires giving prelude the proper arg, then putting the whole COM/pichecker routine into a slice
#       loop bounded by fslsplit and fslmerge. And lots of testing.
#   - get rid of or reduce smoothing on fieldmap creation


printUsage() {
cat << EOF

    $(basename $0)
    --------

    This script creates properly scaled B_0 field-maps for use with the FSL
      FUGUE or FLIRT tools, which are called by epi_reg, FEAT, and MELODIC
      when doing fieldmap based EPI distortion correction.

    May also be used with the 'epiunwarp' script to do distortion
      correction as a standalone step or generate pixelshift maps in EPI space.

    Requires:
      - dcm2niix (part of MRIcroGL, use 14-July-2017 or later: https://www.nitrc.org/frs/?group_id=889)
      - FSL (https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FslInstallation)
      - AFNI (https://afni.nimh.nih.gov/pub/dist/doc/htmldoc/background_install/install_instructs)
      - niftimpri script (ask Andrew)
      - optional: .bash_script_messages in home directory (ask Andrew)

    Usage: $(basename $0) [options] te1_dir te2_dir

    Required positional arguments:
        te1_dir : directory of GRE dicoms with *shorter* TE (e.g. 4.55 ms)
        te2_dir : directory of GRE dicoms with *longer* TE (e.g. 6.82 ms)

    Options:
           --3d : smooth in 3D (good for heads)
          --bet : brain extraction with bet instead of masking with 3dAutomask
        -o pref : use {pref} for B0map prefix instead of {te1_dir}-B0map
      --noclean : keep temporary files

    Examples:
      Muscle images with few slices:
          $(basename $0) gre455 gre682
      Brains:
          $(basename $0) --3d --bet -o B0map gre5 gre8

EOF
}
# removed, for now: -s : perform prelude unwarping slice-at-a-time (good for few slices)


# Robust script options
set -o nounset    # fail on unset variables
set -o errexit    # fail on non-zero return values
set -o pipefail   # fail if any piped command fails


# Default values
docleanup=True
baseDir="$PWD"
uwdir="g"
threedee=False
useBet=False
# prelArgs=("")                   # empty array
declare -r gamma=267.51         # 10^6 rad/s/T
declare -r gammabar=42.576      # MHz/T
declare -r pi=$(python -c "from math import pi; print(pi)")
declare -r piBy2=$(python -c "from math import pi; print(pi/2.0)")


# Red error messages, etc
[[ -f ~/.bash_script_messages ]] \
  && source ~/.bash_script_messages \
  || scriptEcho() { shift $(($#-1)); echo "$1"; }


# Parse arguments
[[ $# -lt 2 ]] && { printUsage; exit 1; }
while [[ $# -gt 2 ]]; do
   case $1 in
            --3d ) threedee=True;;
           --bet ) useBet=True;;
              -o ) outPrefix="${2}"; shift;;
#             -s ) indepSlices=True; prelArgs+=("-s");;        # add element to array
       --noclean ) docleanup=False;;
     --help | -h ) printUsage; exit 0;;
               * ) scriptEcho -e "unrecognized option '$1'"; printUsage; exit 1;;
   esac
   shift
done

# Parse required args and check validity
te1Dir="${1%/}" && shift
te2Dir="${1%/}" && shift

if [[ -z "${te1Dir}" ]] || [[ ! -d "${te1Dir}" ]] || [[ -z "${te2Dir}" ]] || [[ ! -d "${te2Dir}" ]]; then
    scriptEcho -e "missing or invalid directory: '${te1Dir}' or '${te2Dir}'"
    exit 2
elif [[ $te1Dir == *"/"* ]] || [[ $te2Dir == *"/"* ]]; then  # keep te1/2 dirs in current directory for simplicity
    scriptEcho -e "keep te1/2 Dirs in the current directory. Not: ${te1Dir}, ${te2Dir}"
    exit 2
fi

# Test whether user wants .nii or .nii.gz
if [[ $FSLOUTPUTTYPE == NIFTI_GZ ]]; then
    zflag=y

    # temporarily turn off gzip to speed up processing of intermediate files
    export FSLOUTPUTTYPE=NIFTI

elif [[ $FSLOUTPUTTYPE == NIFTI ]]; then
    zflag=n

else
    echo "unknown FSLOUTPUTTYPE: $FSLOUTPUTTYPE"
    exit 2
fi

# Job management: wait for two child processes, testing to ensure output files are created
function wait_for_two {
    # takes three arguments: 1: function name; 2,3: two images to check for
    wait
    [[ $(imtest "$2") -eq 1 ]] && [[ $(imtest "$3") -eq 1 ]] \
      || { scriptEcho -e "$1 had a problem."; exit 2; }
}

# Get/Create tmp directory and output file naming
if [[ -z "${outPrefix:-}" ]]; then
    outPrefix="${te1Dir%%-*}-B0map"
fi
tmpDir=$(mktemp -d -t "ca.spinup.$(basename $0)_pid$$.XXXXX")
[[ -d "${tmpDir}" ]] || { scriptEcho -e "Problem creating temp directory: ${tmpDir}"; exit 2; }

# Output to terminal with scriptEcho and write to log
function echo_log {
    scriptEcho "$1" && echo "$1" >> "${tmpDir}/${outPrefix}_info.txt"
}


# Determine GRE TEs and difference
te1Dicom="$(find "${te1Dir}" -name '[^.]*.MR.dcm' -print -quit)"
te1=$(dicom_hinfo -no_name -tag 0018,0081 "$te1Dicom")
te2Dicom="$(find "${te2Dir}" -name '[^.]*.MR.dcm' -print -quit)"
te2=$(dicom_hinfo -no_name -tag 0018,0081 "$te2Dicom")

tediff=$(python -c "print('{:01.6f}'.format(($te1 - $te2)/1000.0))")

if [[ $(python -c "print($tediff < 0)") != True ]]; then
    scriptEcho -e "GRE TE difference ($tediff) should be a negative value."
    exit 2
else
    echo_log "GRE TE difference was: ${te1} ms - ${te2} ms = $tediff s"
fi


# make NIFTIs
if [[ $(imtest "$te1Dir") -ne 1 ]]; then
    scriptEcho "Making NIFTIs ..."
    dcm2niix -o . -f '%f' -b n -z $zflag "$te1Dir"
    dcm2niix -o . -f '%f' -b n -z $zflag "$te2Dir"
fi


# Work in tmp directory until clean-up phase
imcp "$te1Dir" "${tmpDir}/gre1"
imcp "$te2Dir" "${tmpDir}/gre2"
cd "$tmpDir"


# Split NIFTIs into component (Mag-Phase-Real-Imag) parts
scriptEcho "Splitting NIFTIs ..."
niftimpri gre1
niftimpri gre2
immv gre1-phase gre1-phase_scanner
immv gre2-phase gre2-phase_scanner

# Scale by 1/1000 and add pi to get 0 to 2*pi rad scaling
#fslmaths "gre1-phase" -div 1000 -add $pi "gre1-phase-rad" -odt float
#fslmaths "gre2-phase" -div 1000 -add $pi "gre2-phase-rad" -odt float

# Phase images from real and imaginary,
#   since scanner phase images have had filtering/smoothing applied and are strange...
#   N.B. it seems newer scanner phase images are just fine, but I'll still use this.
for im in gre1 gre2; do
    3dcalc -a ${im}-real.nii \
           -b ${im}-imag.nii \
           -expr "atan2(b,a) + $pi" \
           -prefix ${im}-phase_ri.nii \
           -datum float &
done

wait_for_two 3dcalc gre1-phase_ri gre2-phase_ri


# Do 6 DOF registration of the two GRE (mag) images for later alignment
scriptEcho "Co-registering GRE volumes (in parallel) ..."

# First reg gre2 to gre1 and generate mean image to be target for both regs
flirtArgs=(-cost corratio -interp spline -datatype float)
if [[ $threedee == True ]]; then
    flirtArgs+=(-dof 6)
else
    flirtArgs+=(-2D -schedule $FSLDIR/etc/flirtsch/sch2D_3dof)
fi

flirt -in gre2-mag \
      -ref gre1-mag \
      ${flirtArgs[@]} \
      -omat gre2-to1.xfm \
      -out gre2-mag-to1

fslmaths gre1-mag \
         -add gre2-mag-to1 \
         -div 2.0 \
         gre0-mag \
         -odt float

# Now register both gre images to the mean gre0 image
for im in gre1 gre2; do
    flirt -in ${im}-mag \
          -ref gre0-mag \
          ${flirtArgs[@]} \
          -omat ${im}-to0.xfm \
          -out ${im}-mag-to0 &
done

wait_for_two flirt-mag gre1-mag-to0 gre2-mag-to0


# Re-create the gre0 mean image, and create brain/leg mask in common space
fslmaths gre1-mag-to0 \
         -add gre2-mag-to0 \
         -div 2.0 \
         gre0-mag \
         -odt float

if [[ $useBet == True ]]; then
    bet gre0-mag \
        gre0-mag_brain \
        -m \
        -f 0.5 \
        -R

    fslmaths gre0-mag_brain_mask \
             -fillh \
             gre0-mag_brain_mask \
             -odt int

    # Normalized names across 3D/2D
    imln gre0-mag_brain gre0-mag-am
    imln gre0-mag_brain_mask gre0-mask
else
    3dAutomask -q -clfrac 0.2 \
               -prefix gre0-mask.nii \
               gre0-mag.nii

    fslmaths gre0-mask \
             -fillh \
             gre0-mask \
             -odt int

    fslmaths gre0-mag \
             -mul gre0-mask \
             gre0-mag-am \
             -odt float
fi


# N.B. Fugue docs indicate that masks should err on the small side, if
#   anything, to avoid trying to get phase values from low SNR areas.
#
# SNR from diff image: sqrt(2)*S/sigma. Therefore requiring SNR of 4-5 (Rose
#   criterion, see Bushberg) implies S of 5*sigma/sqrt(2) = 3.536*sigma

# Find median values and difference for regged images
median1=$(fslstats gre1-mag-to0 -k gre0-mask -p 50)
median2=$(fslstats gre2-mag-to0 -k gre0-mask -p 50)
med_diff=$(python -c "print($median1 - $median2)")

# Find standard deviation of difference image for noise estimate
fslmaths gre1-mag-to0 -sub $med_diff -sub gre2-mag-to0 gre12diff -odt float
noise_sigma=$(fslstats gre12diff -k gre0-mask -s)

# Threshold mask
sig_thresh=$(python -c "print(3.536*$noise_sigma)")     # use for both images

for im in gre1 gre2; do
    fslmaths ${im}-mag \
             -thr $sig_thresh \
             -bin \
             ${im}-thrmask \
             -odt int &
done
wait


# Create a mask of each GRE image

# Dilation/erosion kernels -- not used now since I don't dilate
# if [[ $threedee == True ]]; then
#     # add/sub a 4 mm shell
#     # kernArgs=(-kernel box 10)
#     kernArgs=(-kernel sphere 5)
# else
#     # add/sub 2 voxel ring
#     # kernArgs=(-kernel 2D)
#     kernArgs=(-kernel boxv3 5 5 1)
# fi

if [[ $useBet == True ]]; then
    scriptEcho "Masking with bet (in parallel) ..."
    for im in gre1 gre2; do
        bet ${im}-mag \
            ${im}-mag_brain \
            -m \
            -f 0.5 \
            -R &
    done

    wait_for_two bet gre1-mag_brain gre2-mag_brain

    # # dilate masks
    # for im in gre1 gre2; do
    #     fslmaths ${im}-mag_brain_mask \
    #              ${kernArgs[@]} \
    #              -dilD -fillh \
    #              ${im}-mag_brain_dilmask \
    #              -odt int &
    # done

    # wait_for_two fslmaths-dilD gre1-mag_brain_dilmask gre2-mag_brain_dilmask

    # Normalized names across 3D/2D
    for im in gre1 gre2; do
        imln ${im}-mag_brain_mask ${im}-mask
        # imln ${im}-mag_brain_dilmask ${im}-dilmask
    done
else
    # 3dAutomask works better than bet for legs
    # Could also use fslmaths here, although it's not exactly the same:
    #   fslmaths gre1-mag -thrP 20 -bin -kernel 2D -ero -dilF -fillh gre1-mask -odt int
    scriptEcho "Masking with 3dAutomask (in parallel) ..."
    for im in gre1 gre2; do
        3dAutomask -q -clfrac 0.2 \
                   -prefix ${im}-mask.nii \
                   ${im}-mag.nii &
    done

    wait_for_two 3dAutomask gre1-mask gre2-mask

    # # dilate masks
    # for im in gre1 gre2; do
    #     fslmaths ${im}-mask \
    #              ${kernArgs[@]} \
    #              -dilD -fillh \
    #              ${im}-dilmask \
    #              -odt int &
    # done

    # wait_for_two fslmaths-dilD gre1-dilmask gre2-dilmask
fi

# Only voxels where there is adequate SNR have valid phase
for im in gre1 gre2; do
    fslmaths ${im}-mask \
             -mul ${im}-thrmask \
             ${im}-mask \
             -odt int
done


# Use Prelude to unwrap the phase images (use unaligned/uninterpolated images here)
scriptEcho "Unwrapping phase images (in parallel) ..."
[[ $threedee == True ]] || { scriptEcho -w "prelude with removeramps not yet tested in 2D"; }

for im in gre1 gre2; do
    prelude --removeramps -a ${im}-mag \
                          -p ${im}-phase_ri \
                          -m ${im}-mask \
                          -o ${im}-phase_ri-uw-am &
done

wait_for_two prelude gre1-phase_ri-uw-am gre2-phase_ri-uw-am


# Apply the reg to the (unwrapped) phase maps as well
#   This is a bit sketchy:
#   NN interp could be used to prevent smoothing across hard boundaries.
#   However, in practice this is the same as not doing registration when
#   the displacements are small (tested and confirmed in real data). Sinc
#   seems to be a safe way to go in the data, though it does introduce
#   weirdness at the boundary of our (dilated) mask and the background.
#   Overall, it improved the corratio of epiunwarped data a bit to use the
#   sinc interpolated phase maps, even for small disp.
flirtArgs[3]=sinc
# flirtArgs[3]=nearestneighbour
for im in gre1 gre2; do
    flirt -in ${im}-phase_ri-uw-am \
          -ref gre0-mag \
          ${flirtArgs[@]} \
          -applyxfm -init ${im}-to0.xfm \
          -out ${im}-phase_ri-uw-am-to0 &
done
wait_for_two flirt-phase gre1-phase_ri-uw-am-to0 gre2-phase_ri-uw-am-to0

# Also apply the reg to the mask to get a correction factor for the brain boundary
#   N.B. this correction technique (gleaned from Jenkinson on the FSL mailing list for
#   correcting smoothing in a mask) has the benefit of interpolating many voxels out
#   into empty space, which is handy.
for im in gre1 gre2; do
    flirt -in ${im}-mask \
          -ref gre0-mag \
          ${flirtArgs[@]} \
          -applyxfm -init ${im}-to0.xfm \
          -out ${im}-mask-to0 &
done
wait_for_two flirt-mask gre1-mask-to0 gre2-mask-to0

for im in gre1 gre2; do
    fslmaths ${im}-phase_ri-uw-am-to0 \
             -div ${im}-mask-to0 \
             ${im}-phase_ri-uw-am-crct-to0 \
             -odt float &
done
wait_for_two fslmaths-phcorr gre1-phase_ri-uw-am-crct-to0 gre2-phase_ri-uw-am-crct-to0


# Regularization of unwrapped phase images
#  N.B. blurring of phase images changes the values hugely -- makes the two maps
#    closer together near the margins. Using median filter instead.

# Median filtering to despike and deal with boundaries
#   this also fills small gaps effectively, though it
#   does depress the values somewhat on the margins
if [[ $threedee == True ]]; then
    kernel_args=(-kernel 3D)
else
    kernel_args=(-kernel 2D)
fi

for im in gre1 gre2; do
    fslmaths ${im}-phase_ri-uw-am-crct-to0 \
             ${kernel_args[@]} \
             -fmedian \
             ${im}-phase_ri-uw-am-filt-to0 \
             -odt float &
done

wait_for_two fslmaths-filt gre1-phase_ri-uw-am-filt-to0 gre2-phase_ri-uw-am-filt-to0


# N.B. median filter fills small holes with seemingly valid values that we don't want to lose
#   apply the filter to gre0 as well and see if we can get some voxels back
fslmaths gre0-mag-am \
         ${kernel_args[@]} \
         -fmedian \
         gre0-mag-am-filt \
         -odt float

fslmaths gre0-mag-am-filt \
         -thr $sig_thresh \
         -bin \
         gre0-thrmask1 \
         -odt int &

fslmaths gre0-mag-am \
         -thr $sig_thresh \
         -bin \
         gre0-thrmask2 \
         -odt int &

wait_for_two fslmaths-thresh gre0-thrmask1 gre0-thrmask2

# Best, safe mask for later steps
fslmaths gre0-thrmask1 \
         -add gre0-thrmask2 \
         -mul gre0-mask \
         -bin \
         ${kernel_args[@]} \
         -ero \
         gre0-mask \
         -odt int


# Mask magnitude and phase images with common mask for validity and consistency of outputs
for im in gre1 gre2; do
    fslmaths ${im}-mag-to0 \
             -mul gre0-mask \
             ${im}-mag-am-to0 \
             -odt float &

    fslmaths ${im}-phase_ri-uw-am-filt-to0 \
             -mul gre0-mask \
             ${im}-phase_ri-uw-am-filt-to0 \
             -odt float &
done
wait


# Correct phase images so the offset between the two acquisitions is less than 2*pi
scriptEcho "Checking for (2)pi phase differences ..."
function pichecker {
    local phaseTarget=$1
    local phaseToCheck=$2
    local phaseThresh=$3
    local phaseImage="$(remove_ext "$4")"     # with COM phase = phaseToCheck
    # echo "            DEBUG: phaseImage: $phaseImage" >&2
    local phaseImagePref=${phaseImage%-pichecked}

    diffFactor=$(python -c "print(abs($phaseToCheck - $phaseTarget)//$phaseThresh)")                       # floor division
    diffSign=$(python -c "from math import copysign; print(copysign(1, ${phaseTarget}-${phaseToCheck}))")  # compare values, return sign
    diffModulus=$(python -c "print((${diffFactor}*${phaseThresh} + 1e-5)%$pi - 1e-5)")                     # ensure multiples of pi are added
    phaseAddition=$(python -c "print(${diffSign}*(${phaseThresh}*${diffFactor} + ${diffModulus}))")

    echo "            Adding $phaseAddition to $phaseImage ..."  >&2
    fslmaths "$phaseImage" \
             -add $phaseAddition \
             "${phaseImagePref}-pichecked" \
             -odt float
    echo "$(python -c "print($phaseToCheck + $phaseAddition)")"     # return value -> new COM phase
}


# Find centre of mass  -- this and following would go in slice-by-slice loop
scriptEcho -n "Magnitude COM: "
centreCoords=$(fslstats gre0-mag-am -C)

cxCoord=$(printf "${centreCoords% }" | sed -E "s/([0-9]+)\.[0-9]+ ([0-9]+)\.[0-9]+ ([0-9]+)\.[0-9]+/\1/")
cyCoord=$(printf "${centreCoords% }" | sed -E "s/([0-9]+)\.[0-9]+ ([0-9]+)\.[0-9]+ ([0-9]+)\.[0-9]+/\2/")
czCoord=$(printf "${centreCoords% }" | sed -E "s/([0-9]+)\.[0-9]+ ([0-9]+)\.[0-9]+ ([0-9]+)\.[0-9]+/\3/")
echo "$cxCoord,$cyCoord,$czCoord"

# find mean phase values at centre of mass ROI
if [[ $threedee == True ]]; then
    roiArgs=(-roi $((cxCoord-3)) 6 $((cyCoord-3)) 6 $((czCoord-3)) 6 0 1)
else
    roiArgs=(-roi $((cxCoord-3)) 6 $((cyCoord-3)) 6 $((czCoord)) 1 0 1)
fi
fslmaths gre0-mag-am \
         ${roiArgs[@]} \
         gre0-centre_roi \
         -odt int

phase_te1=$(fslstats gre1-phase_ri-uw-am-filt-to0 -k gre0-centre_roi -m | tr -d '[:blank:]')
phase_te2=$(fslstats gre2-phase_ri-uw-am-filt-to0 -k gre0-centre_roi -m | tr -d '[:blank:]')

echo_log "Pre check: at COM, phase_te1=${phase_te1}; phase_te2=${phase_te2}"
echo_log "           delta = $(python -c "print(${phase_te1} - ${phase_te2})") rad"

# Check the fieldmaps and add/sub multiples of pi if necessary to get them between 0 and 2pi.  Probably get each slice mean to btw 0 and 2pi first, then subtract.
phase_te1=$(pichecker 0 $phase_te1 $pi gre1-phase_ri-uw-am-filt-to0)              # make phase_te1 between -pi and pi
phase_te2=$(pichecker $phase_te1 $phase_te2 $piBy2 gre2-phase_ri-uw-am-filt-to0)   # make phase_te2 within pi/2 of phase_te1

echo_log "Post check: at COM, phase_te1=${phase_te1}; phase_te2=${phase_te2}"
echo_log "            delta = $(python -c "print(${phase_te1} - ${phase_te2})") rad"
scriptEcho "            (typical delta around $(python -c "print('{:01.3f}'.format(0.1*${tediff}/-0.00227))") rad)"


# Create fieldmap using fugue from merged phase volumes
#   Consider other regularization like despiking, median filtering here
#   The field-maps without regularization have pronounced striping
scriptEcho "Creating fieldmaps ..."
if [[ $threedee == True ]]; then
    regl_args=(--median --smooth3=1.25)
else
    regl_args=(--median --smooth2=1)
fi

fslmerge -t gre_combo-phase_ri-uw-am-filt-to0-pichecked \
            gre1-phase_ri-uw-am-filt-to0-pichecked \
            gre2-phase_ri-uw-am-filt-to0-pichecked

fugue --mask=gre0-mask \
      --asym=$tediff \
      ${regl_args[@]} \
      -p gre_combo-phase_ri-uw-am-filt-to0-pichecked \
      --savefmap="${outPrefix}-rads"


# Create fieldmap by direct calculation
#   N.B.  would give units of µT
if [[ $threedee == True ]]; then
    smooth_args=(-s 1.25)
else
    smooth_args=(-s 1.25)
fi
fslmaths gre2-phase_ri-uw-am-filt-to0-pichecked \
    -sub gre1-phase_ri-uw-am-filt-to0-pichecked \
    -div $tediff \
    -div $gammabar \
    ${kernel_args[@]} \
    -fmedian \
    -mul gre0-mask \
    ${smooth_args[@]} \
    "${outPrefix}-µT" \
    -odt float

# smooth only within mask
fslmaths gre0-mask ${smooth_args[@]} corr_factor -odt float
fslmaths "${outPrefix}-µT" -div corr_factor -mul gre0-mask "${outPrefix}-µT" -odt float


# Move files to base directory, gzip if required, and clean up
if [[ $useBet == True ]]; then
    outsuffs=(mag mag_brain mag_brain_mask)
else
    outsuffs=(mag mag-am mag-amask)
fi

immv gre0-mag "${outPrefix}-${outsuffs[0]}"
immv gre0-mag-am "${outPrefix}-${outsuffs[1]}"
immv gre0-mask "${outPrefix}-${outsuffs[2]}"

if [[ $zflag == y ]]; then
    # gzip was preferred; make them!
    export FSLOUTPUTTYPE=NIFTI_GZ

    gzip -c gre1-phase_ri-uw-am-filt-to0-pichecked.nii > "${baseDir}/${te1Dir}-uwphase.nii.gz" &
    gzip -c gre2-phase_ri-uw-am-filt-to0-pichecked.nii > "${baseDir}/${te2Dir}-uwphase.nii.gz" &

    gzip -c gre1-mag-am-to0.nii > "${baseDir}/${te1Dir}-${outsuffs[1]}.nii.gz" &
    gzip -c gre2-mag-am-to0.nii > "${baseDir}/${te2Dir}-${outsuffs[1]}.nii.gz" &

    for f in "${outPrefix}"*.nii; do
        gzip -c "$f" > "${baseDir}/${f}.gz" &
    done
    wait
else
    immv gre1-phase_ri-uw-am-filt-to0-pichecked "${baseDir}/${te1Dir}-uwphase"
    immv gre2-phase_ri-uw-am-filt-to0-pichecked "${baseDir}/${te2Dir}-uwphase"
    immv gre1-mag-am-to0 "${baseDir}/${te1Dir}-${outsuffs[1]}"
    immv gre2-mag-am-to0 "${baseDir}/${te2Dir}-${outsuffs[1]}"
    immv $(imglob "${outPrefix}"\*) "${baseDir}/"
fi

/bin/mv "${outPrefix}_info.txt" "${baseDir}/"

cd "$baseDir"

if [[ $docleanup == True ]]; then
    scriptEcho "Deleting $tmpDir ..."
    /bin/rm -r "$tmpDir"
else
    scriptEcho "Temp files are in $tmpDir"
fi
